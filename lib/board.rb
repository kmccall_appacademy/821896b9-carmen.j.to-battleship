class Board
  attr_reader :grid
  DISPLAY_HASH = {
    nil => " ",
    :s => " ",
    :x => "x"
  }

  def self.default_grid
    Array.new(10) { Array.new(10) { nil } }
  end

  def self.random
    self.new(self.default_grid, true)
  end

  def initialize(grid=Board.default_grid)
    @grid = grid
    @ship_count = 0
  end

  def [](pos)
    i, j = pos[0], pos[1]
    @grid[i][j]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def count
    @grid.flatten.count(:s)
  end

  def display
    header = (0..9).to_a.join("  ")
    p "  #{header}"
    grid.each_with_index { |row, i| display_row(row, i) }
  end

  def display_row(row, i)
    chars = row.map { |el| DISPLAY_HASH[el] }.join("  ")
    p "#{i} #{chars}"
  end

  def empty?(pos=nil)
    if pos.nil?
      return count == 0
    end
    i, j = pos[0], pos[1]
    @grid[i][j].nil?
  end

  def full?
    @grid.flatten.length == count
  end

  def in_range?(pos)
    pos.all? { |x| x.between?(0, grid.length - 1) }
  end

  def place_random_ship
    raise "hell" if full?
    pos = random_pos

    until empty?(pos)
      pos = random_pos
    end

    self[pos] = :s
  end

  def randomize(count = 10)
    count.times { place_random_ship }
  end

  def random_pos
    [rand(size), rand(size)]
  end

  def size
    grid.length
  end

  def won?
    grid.flatten.none? { |el| el == :s }
  end
 end
